# Minesweeper Java

Simple Minesweeper written usnig Java Core, Swing.

## Description
Just build, open jar file and enjoy playing a classic minesweeper with 9x9 field.

![Minesweeper](MineSweeper.png "JMinesweeper")
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://gitlab.com/ABelousow1/minesweeper-java/-/blob/main/MineSweeper.mp4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

