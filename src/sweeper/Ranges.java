package sweeper;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Ranges {
    private static Coord _size;
    private static List<Coord> allCoords;

    private static Random random = new Random();

    public static void setSize(Coord size){
        _size = size;
        allCoords = new ArrayList<>();
        for (int y = 0; y < size.y; ++y){
            for (int x = 0; x < size.x; ++x){
                allCoords.add(new Coord(y, x));
            }
        }
    }

    public static Coord getSize(){
        return _size;
    }

    public static List<Coord> getAllCoords(){
        return allCoords;
    }

    static boolean inRange(Coord coord){
        return coord.x >= 0 && coord.x < _size.x &&
               coord.y >= 0 && coord.y < _size.y;
    }

    static Coord getRandomCoord(){
        return new Coord(random.nextInt(_size.x), random.nextInt(_size.y));
    }

    static List<Coord> getCoordsAround(Coord coord){
        Coord around;
        List<Coord> list = new ArrayList<>();
        for (int x = coord.x - 1; x <= coord.x + 1; ++x){
            for (int y = coord.y - 1; y <= coord.y + 1; ++y){
                if (inRange(around = new Coord(x, y))){
                    if (!around.equals(coord)){
                        list.add(around);
                    }
                }
            }
        }
        return list;
    }
}
