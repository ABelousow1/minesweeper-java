package sweeper;

class Flag {
    private Matrix flagMap;
    private int totalFlags;
    private int countOfClosedBoxes;

    void start(){
        flagMap = new Matrix(Box.CLOSED);
        countOfClosedBoxes = Ranges.getSize().x * Ranges.getSize().y;
    }

    Box get(Coord coord){
        return flagMap.get(coord);
    }

    void setOpenedToBox(Coord coord){
        flagMap.set(coord, Box.OPENED);
        countOfClosedBoxes--;
    }

    void toggleFlaggedToBox(Coord coord){
        switch (flagMap.get(coord)){
            case FLAGGED -> setClosedToBox(coord);
            case CLOSED -> setFlaggedToBox(coord);
        }
    }

    void setNobombToFlaggedSafeBox(Coord coord){
        if (flagMap.get(coord) == Box.FLAGGED){
            flagMap.set(coord, Box.NOBOMB);
        }
    }

    void setOpenedToClosedBombBox(Coord coord){
        if (flagMap.get(coord) == Box.CLOSED){
            flagMap.set(coord, Box.OPENED);
        }
    }

    int getCountOfClosedBoxes(){
        return countOfClosedBoxes;
    }
    int getTotalFlags(){
        return totalFlags;
    }
    void setFlaggedToBox(Coord coord){
        flagMap.set(coord, Box.FLAGGED);
    }
    private void setClosedToBox(Coord coord){
        flagMap.set(coord, Box.CLOSED);
    }

    void setBombedToBox(Coord coord) {
        flagMap.set(coord, Box.BOMBED);
    }

    int getCountOfFlaggedBoxesAround(Coord coord) {
        int count = 0;
        for (Coord around : Ranges.getCoordsAround(coord)){
            if (flagMap.get(around) == Box.FLAGGED)
                count++;
        }
        return count;
    }
}
